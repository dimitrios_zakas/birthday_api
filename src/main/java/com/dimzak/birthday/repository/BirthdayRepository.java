package com.dimzak.birthday.repository;

import com.dimzak.birthday.birthday.Birthday;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BirthdayRepository extends CrudRepository<Birthday, String> {

}
