package com.dimzak.birthday.web;

import javax.validation.constraints.NotNull;

public class AddBirthDayRequest {

    @NotNull
    private String dateOfBirth;

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
