/*
 * Copyright 2012-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dimzak.birthday.web;


import com.dimzak.birthday.birthday.BirthDayChecker;
import com.dimzak.birthday.birthday.Birthday;
import com.dimzak.birthday.repository.BirthdayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

/**
 * Controller for birthday endpoints
 */
@RestController
public class BirthdayController {

	@Autowired
	BirthdayRepository birthdayRepository;

	@Autowired
	BirthDayChecker birthDayChecker;

	/**
	 * Add birthday for a user and return 204
	 * @param user
	 * @param addBirthDayRequest
	 * @return
	 */
	@PutMapping("/hello/{user}")
	public ResponseEntity addBirthday(@PathVariable("user") String user, @RequestBody(required = true) AddBirthDayRequest addBirthDayRequest) {
		try {
			birthdayRepository.save(new Birthday(user, addBirthDayRequest.getDateOfBirth()));
		} catch (ParseException e) {
			e.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Get personalised message for a user's birthday
	 * @param user
	 * @return
	 */
	@GetMapping("/hello/{user}")
	public Object getUser(@PathVariable("user") String user) {
		Birthday birthday = birthdayRepository.findById(user).orElse(new Birthday());
		if(birthday.getName()!=null) {
			return birthDayChecker.getBirthDayMessage(birthday);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}

}
