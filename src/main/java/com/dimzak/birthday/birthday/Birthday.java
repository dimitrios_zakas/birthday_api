package com.dimzak.birthday.birthday;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Entity
public class Birthday {

    @Id
    private String name;

    private LocalDate dateOfBirth;

    public Birthday() {

    }

    public Birthday(String name, LocalDate dateOfBirth) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Constructor to convert automatically dob to LocalDate
     * @param name
     * @param dateOfBirth
     * @throws ParseException
     */
    public Birthday(String name, String dateOfBirth) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate dobLocalDate = LocalDate.parse(dateOfBirth, formatter);

        this.name = name;
        this.dateOfBirth = dobLocalDate;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
