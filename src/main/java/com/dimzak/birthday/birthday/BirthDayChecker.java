package com.dimzak.birthday.birthday;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class BirthDayChecker {

    /**
     * Get personalised message depending on user's birthday
     * @param birthday
     * @return
     */
    public String getBirthDayMessage(Birthday birthday) {
        LocalDate today = LocalDate.now();

        if(today.getMonth()==birthday.getDateOfBirth().getMonth() && today.getDayOfMonth()==birthday.getDateOfBirth().getDayOfMonth()) {
            return "Hello " + birthday.getName() + "! Happy birthday!";
        }
        else if(birthday.getDateOfBirth().getDayOfYear() - today.getDayOfYear() == 5) {
            return "Hello "+ birthday.getName() + "! Your birthday is in 5 days";
        }
        return "Hello " + birthday.getName() + "! It's not your birthday yet";
    }
}
