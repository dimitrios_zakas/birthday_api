package com.dimzak.birthday.birthday.integration_tests;

import com.dimzak.birthday.birthday.Birthday;
import com.dimzak.birthday.repository.BirthdayRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BirthdayRepositoryITTest {

    @Autowired
    private BirthdayRepository birthdayRepository;

    @Test
    public void testGetAllBirthdays() {
        List<Birthday> allBirthdays = (List<Birthday>) birthdayRepository.findAll();
        // Shouldn't be empty as Dimitrios's birthday is installed on startup
        assert(allBirthdays.size()==1 && allBirthdays.get(0).getName().equals("Dimitrios"));
    }
}
