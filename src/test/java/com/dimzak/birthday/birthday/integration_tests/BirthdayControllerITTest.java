package com.dimzak.birthday.birthday.integration_tests;

import com.dimzak.birthday.web.BirthdayController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cglib.core.Local;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BirthdayControllerITTest {

    @Autowired
    private BirthdayController birthdayController;

    @Autowired
    protected WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.mockMvc = standaloneSetup(this.birthdayController).build();// Standalone context
        // mockMvc = MockMvcBuilders.webAppContextSetup(wac)
        // .build();
    }

    @Test
    public void testAddBirthday() throws Exception {
        mockMvc.perform(put("/hello/Alex").content("{\"dateOfBirth\":\"2000-10-10\"}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));

    }

    @Test
    public void testGetBirthday() throws Exception {

        // Birthday
        mockMvc.perform(put("/hello/Alex").content("{\"dateOfBirth\": "+ "\"" +LocalDate.now().toString() + "\"" +"}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));

        mockMvc.perform(get("/hello/Alex").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("Hello Alex! Happy birthday!"));

        // Birthday - 4
        mockMvc.perform(put("/hello/Alex2").content("{\"dateOfBirth\": "+ "\"" +LocalDate.now().plusDays(5).toString() + "\"" +"}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));

        mockMvc.perform(get("/hello/Alex2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("Hello Alex2! Your birthday is in 5 days"));

        // Not Birthday
        mockMvc.perform(put("/hello/Alex3").content("{\"dateOfBirth\": "+ "\"" +LocalDate.now().minusDays(1).toString() + "\"" +"}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));

        mockMvc.perform(get("/hello/Alex3").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("Hello Alex3! It's not your birthday yet"));


    }
}
