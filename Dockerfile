FROM openjdk:8-jre-alpine

# Port to outside world
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=target/birthday-api-*.jar

# Add the application's jar to the container
ADD ${JAR_FILE} birthday-api.jar

ENTRYPOINT ["java","-Dspring.profiles=prod","-jar","/birthday-api.jar"]
