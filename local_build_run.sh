#!/usr/bin/env bash
mvn spring-boot:run -Dspring.profiles.active=dev  -Drun.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8001"

