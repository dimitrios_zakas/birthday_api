Part 1
====

* Application code is written in `Java/Spring Boot/Undertow` and can be found in [src/](https://bitbucket.org/dimitrios_zakas/birthday_api/src/master/src/) directory.  
* To run locally, use `./local_build_run.sh` which utilises an `h2` in memory database and no setup is needed.  
* Integration tests can be found in [src/test](https://bitbucket.org/dimitrios_zakas/birthday_api/src/master/src/test/) directory.

Part 2
====
* Architectural diagram can be found in `diagrams` directory. 
* System is based on `AWS` infrastructure  and services used (among others) are `VPC`, `EC2`, `RDS`, `ASG`.

Part 3
====
* To deploy without downtime (using `rolling upgrades`, `connection draining`, etc), [Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) is used  and initial setup is needed.
* `RDS` needs also to be initialised before deployment, with `Multi A-Z` enabled as described [here](https://aws.amazon.com/getting-started/tutorials/create-connect-postgresql-db/) and app's connection details need to be configured [here](https://bitbucket.org/dimitrios_zakas/birthday_api/src/master/src/main/resources/application-prod.properties) .
* Database migration is handled by `Part 1`.
* `eb` variables can then be used in `deploy.sh` script which creates an archive of the executable application along with specific eb configuration from [.ebextensions](https://bitbucket.org/dimitrios_zakas/birthday_api/src/master/.ebextensions/) and deploys to `Elastic Beanstalk`.



