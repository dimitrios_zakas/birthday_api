#!/usr/bin/env bash
./build.sh

# Version from pom will be used to tag eb deploy
version=$(printf 'VERSION=${project.version}\n0\n' | mvn org.apache.maven.plugins:maven-help-plugin:evaluate | grep '^VERSION' | sed 's/VERSION=//')
exec_bundle=birthday-api-$version.zip

# EB settings
# https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/using-features.managing.html
environment="birthday_api_prod"
s3Bucket="birthday_api"
s3Path="versions/"
s3Key="$s3Path$exec_bundle"
application="birthday_app"
####


zip $exec_bundle -j target/birthday-api-$version.jar
zip  -r $exec_bundle .ebextensions/




# Add the Dockerrun to S3 so that beanstalk can access it
aws s3 cp $exec_bundle s3://$s3Bucket/$s3Path

# Create version
aws elasticbeanstalk create-application-version \
    --application-name $application \
    --version-label "$version" \
    --source-bundle "{\"S3Bucket\":\"$s3Bucket\",\"S3Key\":\"$s3Key\"}"


# Deploy to stage
aws elasticbeanstalk update-environment \
    --environment-name "$environment" \
    --version-label "$version"

while true; do

    app_version=`aws elasticbeanstalk describe-environments --application-name $application --environment-name $environment --query "Environments[*].VersionLabel" --output text`
    status=`aws elasticbeanstalk describe-environments --application-name $application --environment-name $environment --query "Environments[*].Status" --output text`
    if [ "$status" != "Ready" ]; then
        echo "System not ready [$status]. Trying again..."
        sleep 10
        # rerun version/status
        continue
    else
        echo "System ready: [$status]"
    fi
    break
done